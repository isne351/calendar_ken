<?php
class database 
{

	private $config;

	public function __construct()
	{

		$this->instance = get_instance();

        $this->config = get_config('database');  

	}

    public function connect($config = 'main',$return = false,$chrset = 'UTF8')
    {

    	$config =  $this->config[$config];

        $db = new pdo("mysql:host=" . $config['host'] . ";dbname=" .  $config['name'] . ";port=" .  $config['port'] . ";",  $config['username'],  $config['password']);
        $db->query("SET NAMES '" . $chrset . "'");
        $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);       

        if($return == true){
            return $db;
        }else{
            $this->instance->database = $db;
        }
    }

}
