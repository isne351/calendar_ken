<?php
$config['route']['index'] = 'member/login'; 
$config['route']['dashboard'] = 'dashboard/index'; 

$config['route']['event/([0-9]+)'] = 'content/event'; 
$config['route']['event/by/([0-9]+)'] = 'content/user'; 
$config['route']['event/search'] = 'content/search'; 

$config['route']['login'] = 'member/login'; 
$config['route']['register'] = 'member/register'; 

$config['route']['member/event/insert'] = 'member/evet/insert'; 
$config['route']['member/event/update/([0-9]+)'] = 'member/post/update'; 
$config['route']['profile'] = 'member/profile'; 


$config['route']['action/login'] = 'action/login'; 
$config['route']['action/register'] = 'action/register'; 
$config['route']['action/logout'] = 'action/logout'; 

$config['route']['action/uploadimage'] = 'action/uploadimage';

$config['route']['action/event'] = 'action/event/edit';
$config['route']['action/event/delete'] = 'action/event/delete';

$config['route']['action/comment/insert'] = 'action/comment/insert';
$config['route']['action/comment/update'] = 'action/comment/update';
$config['route']['action/comment/delete'] = 'action/comment/delete';

$config['route']['ajax/get/month'] = 'ajax/get/month'; 
$config['route']['ajax/get/week'] = 'ajax/get/week'; 
$config['route']['ajax/get/day'] = 'ajax/get/day'; 
$config['route']['ajax/get/event'] = 'ajax/get/event';

$config['route']['404'] = 'error/page_not_found'; 

$config['route']['pull'] = 'action/hook';