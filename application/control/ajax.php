<?php
Class ajax extends json_control
{

    public function __construct(){
        parent::__construct();
    }

	public function get($page)
	{

		$this->instance->loader->app->library('calendar');

		$view = $this->instance->template;

		switch($page){

			case 'month':
				$calendar = $this->instance->calendar->month(input::get('date'));
			break;

			case 'week':
				$calendar = $this->instance->calendar->week(input::get('date'));
			break;

			case 'day':
				$calendar = $this->instance->calendar->day(input::get('date'));
			break;

			case 'event':
				$calendar = $this->instance->event->get_by_id(input::get('id'))->to_array();
			break;

		}

		$view->set_var('calendar',$calendar);

	}

}