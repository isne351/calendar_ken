<?php
Class comment_model extends model{

    protected $object = array(
    		'comment_id'=>'',
            'post_id'=>'',
    		'content'=>'',
    		'owner'=>'',
    		'create_date'=>'',
    		);


    public function get_by_postid($post_id,$limit = 0 ,$offset = 0){

        $this->instance->query->where('`post_comment`.`post_id` = :post_id ');

        $this->instance->query->order('DESC','`post_comment`.`create_date`');   

        if($limit > 0 ){
            $this->instance->query->limit($limit,$offset);    
        }
        
        $query = $this->instance->query->exec('select','post_comment');

        $exec = $this->instance->database->prepare($query);
        $exec->bindValue(':post_id',$post_id);

        $result = $exec->execute();

        $fetch = $exec->fetchAll(PDO::FETCH_CLASS);

        $list = array();

        foreach($fetch as $value){
           $list[] = new comment_model($value);
        }

        return $list;

    }

    public function insert(){

        $this->instance->query->set('content');
        $this->instance->query->set('post_id');
        $this->instance->query->set('owner');

        $query = $this->instance->query->exec('insert','post_comment');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':content',$this->content,PDO::PARAM_STR);
            $exec->bindValue(':post_id',$this->post_id,PDO::PARAM_INT);
            $exec->bindValue(':owner',$this->owner,PDO::PARAM_INT);

        $result = $exec->execute();

        $this->comment_id = $this->instance->database->lastInsertId();

        return $result;

    }

    public function delete(){

        $this->instance->query->where('`comment_id` = :comment_id');
        
        $query = $this->instance->query->exec('select','post_comment');

        $exec = $this->instance->database->prepare($query);
                $exec->bindValue(':comment_id',$this->comment_id,PDO::PARAM_INT);
        $result = $exec->execute();

        $fetch = $exec->fetch(PDO::FETCH_OBJ);

        if($fetch->owner == $this->owner){         
            $this->instance->query->where('`comment_id` = :comment_id');
            $this->instance->query->where('`owner` = :owner');

            $query = $this->instance->query->exec('delete','post_info');

            $exec = $this->instance->database->prepare($query);
                $exec->bindValue(':comment_id',$this->comment_id,PDO::PARAM_INT);
                $exec->bindValue(':owner',$this->owner,PDO::PARAM_INT);

            $result = $exec->execute();   
        }else{

            $this->instance->query->where('`post_id` = :post_id');
            if($this->instance->user->level != 'admin'){
                $this->instance->query->where('`owner` = :owner');
            }
            
            $query = $this->instance->query->exec('select','post_info');

            $exec = $this->instance->database->prepare($query);
                    $exec->bindValue(':post_id',$fetch->post_id,PDO::PARAM_INT);
                    if($this->instance->user->level != 'admin'){
                        $exec->bindValue(':owner',$this->owner,PDO::PARAM_INT);
                    }
            $result = $exec->execute();

            $row = $exec->rowCount();

            if($row > 0){
                $this->instance->query->where('`comment_id` = :comment_id');

                $query = $this->instance->query->exec('delete','post_comment');

                $exec = $this->instance->database->prepare($query);
                    $exec->bindValue(':comment_id',$this->comment_id,PDO::PARAM_INT);
                $result = $exec->execute();                   
            }else{
                $this->error = 'you are not onwer!';
                return false;
            }

        }

        return $result;

    }    

}
?>