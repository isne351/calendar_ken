<?php
	class input{
		static public function post($key){
			if(isset($_POST[$key])){
				return $_POST[$key];
			}else{
				return false;
			}
		}

		static public function get($key){
			if(isset($_GET[$key])){
				return $_GET[$key];
			}else{
				return false;
			}
		}

		static public function session($key){
			if(isset($_SESSION[$key])){
				return $_SESSION[$key];
			}else{
				return false;
			}			
		}
	}