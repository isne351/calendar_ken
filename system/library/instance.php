<?php
class instance
{
    protected $object;

    public function __construct(){
    	$this->object = new stdClass();
    }

    public function __set($key,$value)
    {
   		$this->object->$key = $value;

        return $value;
    }

    public function __get($key)
    {
        if(isset($this->object->$key)){
    	   return $this->object->$key;
        }else{
           return false;
        }
    }
}
