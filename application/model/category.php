<?php
Class category_model extends model{

    protected $object = array('category_id'=>'','category_name'=>'');

    public function get_all()
    {

        $this->instance->query->order('DESC','`post_category`.`category_id`'); 
        $query = $this->instance->query->exec('select','post_category');

        $exec = $this->instance->database->prepare($query);

        $result = $exec->execute();

        $fetch = $exec->fetchAll(PDO::FETCH_CLASS);

        foreach($fetch as $value){
            $list[] = new category_model($value);
        }

        return $list;

    }

    public function get_by_id($category_id){

        $this->instance->query->where('`category_id` = :category_id ');

        $query = $this->instance->query->exec('select','post_category');
        
        $exec = $this->instance->database->prepare($query);
        $exec->bindValue(':category_id',$category_id);
        $result = $exec->execute();

        $fetch = $exec->fetch(PDO::FETCH_OBJ);

        $post = new category_model($fetch);
    
        return $post;

    } 

}
?>