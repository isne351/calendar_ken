<?php

class loader{

	private $path;
	private $instace;
	private $base;

	public function __construct($base){
		$this->instace = get_instance();
		$this->base = $base;
	}
	
	private function check(){
		return file_exists($this->path);
	}

	protected function run($variable = array()){
		if($this->check() == true){

			if($variable == true){
				foreach ($variable as $key=>$val){
					$$key=$val;
				} 
			}

			require_once($this->path);
			return true;
		}else{
			return false;
		}
	}

	public function model($name,$create = true,$key = false){

		$n = explode('/',$name);

		$class_name = end($n).'_model';

		$obj = end($n);

		$this->path = $this->base.'/model/'.$name.'.php';

		if($this->run() == true){
			if($create == true){
				if(class_exists($class_name)){
					if($key == false){
						if($this->instace->$obj == false){
							$this->instace->$obj = new $class_name;
						}
					}else{
						$this->instace->$key = new $name;					
					}
				}
			}
		}else{
			throw new Exception('ไม่เจอ model '.$name);
		}	
	}

	public function helper($name,$variable = array()){

		$this->path = $this->base.'helper/'.$name.'.php';

		if($this->run() == false){
			throw new Exception('ไม่เจอ lib '.$class_name);
		}

	}

	public function view($name,$variable = array()){

		$this->path = $this->base.$name.'.html';

		if($this->run($variable) == false){
			throw new Exception('หาไฟล์ view '.$name.' ไม่เจอ');
		}	

	}

	public function library($name,$key = false){

		$n = explode('/',$name);

		$class_name = end($n);
		$this->path = $this->base.'library/'.$name.'.php';

		if($this->run() == true){
			if(class_exists($class_name)){
				if($key == false){
					if($this->instace->$class_name == false){
						$this->instace->$class_name = new $class_name;
					}
				}else{
					$this->instace->$key = new $class_name;					
				}
			}
		}else{
			throw new Exception('ไม่เจอ lib '.$class_name);
		}
	}

	public function control($name){

		$n = explode('/',$name);

		$class_name = end($n);
		$this->path = $this->base.'control/'.$name.'.php';

		if($this->run() == true){
			if(class_exists($class_name)){
				$this->instace->$class_name = new $class_name;
			}
		}else{
			throw new Exception('ไม่เจอ contol '.$name);
		}
	}

	public function __get($key){
		if($this->$key){
			return $this->$key;
		}else{
			return false;
		}
	}

}



?>