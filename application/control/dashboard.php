<?php
Class dashboard extends page_control{

	public function __construct(){
		parent::__construct();

		if($this->instance->user->is_login() == false){
			redirect(base());
		}		

		$this->instance->loader->app->helper('post_block');
	}

	public function index()
	{
		$this->instance->template->set_view('body','calendar/month');
	}


}