<?php
Class action extends action_control
{

	public function register()
	{

		if($this->instance->user->is_login() == true){
			redirect(base('dashboard'));
		}		

		if(input::post('password') == input::post('password_confirm')){

			$this->instance->user->username = input::post('username');
			$this->instance->user->password = input::post('password');

			$this->instance->user->name = input::post('name');
			$this->instance->user->gender = input::post('gender');

			if($this->instance->user->register() == true){
				alert(base('login'),'Register finish!');
			}else{
				result($this->instance->user->get_error());
			}

		}else{
				result('Password != Comfirm Password');			
		}
		
	}

	public function login()
	{

		if($this->instance->user->is_login() == true){
			redirect(base('dashboard'));
		}

		$this->instance->user->username = input::post('username');
		$this->instance->user->password = input::post('password');

		if($this->instance->user->login() == true){
			$_SESSION['login'] = $this->instance->user->user_id;
			redirect(base('dashboard'));
		}else{
			result($this->instance->user->get_error());
		}

	}

	public function logout(){
		session_destroy();
		redirect(base('dashboard'));
	}

	public function event($action){

		$this->instance->loader->app->helper('upload');
		$this->instance->loader->app->model('event');

		switch ($action) {
			case 'edit':

					$this->instance->event->event_id = input::post('event_id');

					$this->instance->event->topic = input::post('title');
					$this->instance->event->content = input::post('detail');

					$this->instance->event->event_start = input::post('start');
					$this->instance->event->event_end = input::post('end');

					$this->instance->event->privacy = input::post('privacy');

					$this->instance->event->owner = $this->instance->user->user_id;

					if(input::post('action') == 'insert'){
						$aciton = $this->instance->event->insert();
					}else{
						$action = $this->instance->event->update();
					}
						
					if($action == true){
						echo 'success';
					}else{
						echo 'fail';
					}

				break;

			case 'delete':

					$this->instance->post->event_id = input::get('event_id');
					$this->instance->post->owner = $this->instance->user->user_id;
					
					if($this->instance->post->delete() == true){
						alert(base(),'Delete success!');
					}else{
						result($this->instance->post->get_error());
					}

				break; 	
		}

	}

	public function comment($action){


		$this->instance->loader->app->model('comment');

		switch ($action) {
			case 'insert':

					$this->instance->comment->content = input::post('content');
					$this->instance->comment->event_id = input::post('event_id');
					$this->instance->comment->owner = $this->instance->user->user_id;

					if($this->instance->comment->insert() == true){
						result('Comment success!');
					}else{
						result($this->instance->comment->get_error());
					}

				break;
			case 'delete':
					$this->instance->comment->comment_id = input::get('comment_id');
					$this->instance->comment->owner = $this->instance->user->user_id;

					if($this->instance->comment->delete() == true){
						result('Comment success!');
					}else{
						result($this->instance->comment->get_error());
					}

				break;
		}

	}

	public function uploadimage(){

		$this->instance->loader->app->helper('upload');

		$image = empty($_FILES['image']) ? '' : $_FILES['image'];

        if (!empty($image))
        {
            if ($image['size'] > 0)
            {
            	echo upload($image);
            }
        }

	}

	public function pull(){
		echo shell_exec('git pull');
	}

}