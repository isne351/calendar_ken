<?php
Class event_model extends model{

    protected $object = array(
    		'event_id'=>'',
    		'topic'=>'',
    		'content'=>'',
            'privacy'=>'private',
    		'owner'=>'',
            'event_start'=>'',
            'event_end'=>'',    		
            'create_date'=>'',
            'editable' => false,
            'comment'=>array()
    		);   

    protected function topic($topic)
    {
        if(empty($topic) == true){
             throw new Exception('invalid topic!');
        }

        return $topic;
    }

    protected function owner($owner){
        if($this->instance->user->user_id == $owner){
            $this->editable = true;
        }

        return $owner;
    }

    protected function privacy($privacy){

        if($privacy == false){
            return 'private';
        }

        $allow = array('public','private');

        if(in_array($privacy,$allow) == false){
            throw new Exception('invalid privacy!');
        }

        return $privacy;
    } 

    public function get_by_day($date)
    {

        $this->instance->query->where('DATE(`event_start`) <= DATE(:date)');
        $this->instance->query->where('DATE(`event_end`) >= DATE(:date)');

        $query = $this->instance->query->exec('select','event_info');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':date',date('Y-m-d',strtotime($date)));
        $result = $exec->execute();

        $fetch = $exec->fetchAll(PDO::FETCH_CLASS);

        $list = array();

        foreach($fetch as $value){
           $list[] = new event_model($value);
        }

        return $list;

    }

    public function get_by_week($date)
    {
        $this->instance->query->where('YEAR(`event_start`) = YEAR(:date)',0);
        $this->instance->query->where('WEEKOFYEAR(`event_start`) = WEEKOFYEAR(:date)',0);

        $this->instance->query->or_where('WEEKOFYEAR(`event_end`) = WEEKOFYEAR(:date)',1);
        $this->instance->query->where('YEAR(`event_end`) = YEAR(:date)',1);

        $query = $this->instance->query->exec('select','event_info');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':date',date('Y-m-d',strtotime($date)));
        $result = $exec->execute();

        $fetch = $exec->fetchAll(PDO::FETCH_CLASS);

        $list = array();

        foreach($fetch as $value){
           $list[] = new event_model($value);
        }

        return $list;
    }

    public function get_by_month($date)
    {
        $this->instance->query->where('YEAR(`event_start`) = YEAR(:date)',0);
        $this->instance->query->where('MONTH(`event_start`) = MONTH(:date)',0);

        $this->instance->query->or_where('MONTH(`event_end`) = MONTH(:date)',1);
        $this->instance->query->where('YEAR(`event_end`) = YEAR(:date)',1);

        $query = $this->instance->query->exec('select','event_info');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':date',date('Y-m-d',strtotime($date)));
        $result = $exec->execute();

        $fetch = $exec->fetchAll(PDO::FETCH_CLASS);

        $list = array();

        foreach($fetch as $value){
           $list[] = new event_model($value);
        }

        return $list;
    }

    public function get_by_id($id)
    {
        $this->instance->query->where('`event_id` = :event_id');

        $query = $this->instance->query->exec('select','event_info');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':event_id',$id);
        $result = $exec->execute();

        $fetch = $exec->fetch(PDO::FETCH_OBJ);

        return new event_model($fetch);
    }

    public function insert(){

        $this->instance->query->set('topic');
        $this->instance->query->set('content');
        $this->instance->query->set('owner');
        $this->instance->query->set('privacy');

        $this->instance->query->set('event_start');
        $this->instance->query->set('event_end');
        $this->instance->query->set('create_date');

        $query = $this->instance->query->exec('insert','event_info');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':topic',$this->topic,PDO::PARAM_STR);
            $exec->bindValue(':content',$this->content,PDO::PARAM_STR);
            $exec->bindValue(':owner',$this->owner,PDO::PARAM_INT);
            $exec->bindValue(':privacy',$this->privacy,PDO::PARAM_INT);

            $exec->bindValue(':event_start',$this->event_start,PDO::PARAM_STR);
            $exec->bindValue(':event_end',$this->event_end,PDO::PARAM_STR);
            $exec->bindValue(':create_date',date('Y-m-d H:i:s'),PDO::PARAM_STR);

        $result = $exec->execute();

        $this->event_id = $this->instance->database->lastInsertId();
        return $result;

    }

    public function update()
    {

        $this->instance->query->set('topic');
        $this->instance->query->set('content');
        $this->instance->query->set('privacy');

        $this->instance->query->set('event_start');
        $this->instance->query->set('event_end');

        $this->instance->query->where('`event_id` = :event_id');

        $query = $this->instance->query->exec('update','event_info');

        $exec = $this->instance->database->prepare($query);
            $exec->bindValue(':topic',$this->topic,PDO::PARAM_STR);
            $exec->bindValue(':content',$this->content,PDO::PARAM_STR);
            $exec->bindValue(':privacy',$this->privacy,PDO::PARAM_INT);

            $exec->bindValue(':event_start',$this->event_start,PDO::PARAM_STR);
            $exec->bindValue(':event_end',$this->event_end,PDO::PARAM_STR);

            $exec->bindValue(':event_id',$this->event_id,PDO::PARAM_INT);

        $result = $exec->execute();

        return $result;

    }

    public function delete(){

    }

}
?>