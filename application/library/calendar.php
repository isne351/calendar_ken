<?php
class calendar extends library
{

    private $event = array();
    private $select;
    protected $next;
    protected $prev;
    protected $calendar;    

    public function __construct(){
        parent::__construct();

        $this->instance->loader->database->connect();
        $this->instance->loader->app->model('event');
        $this->instance->loader->app->model('comment');
        $this->instance->loader->app->model('user');
    }

    private function event_date_diff($array)
    {

        $event = array();

        foreach($array as $ev){

            $day = date('d', strtotime($ev->event_start));

            $month = date('m', strtotime($ev->event_start));

            $year = date('Y', strtotime($ev->event_start));

            $date1=date_create($ev->event_start);
            $date2=date_create($ev->event_end);
            $diff=date_diff($date1,$date2);

            $datediff = $diff->format("%a days");

            for($i = 0;$i <= $datediff ;$i++){
                $tmp = mktime(0, 0, 0, $month, ($day+$i), $year);

                $event[date('Y-m-d',$tmp)][] = $ev->to_array();
            }

        }

        return $event;

    }

    public function month($select = false)
    {
        if ($select == false) {
            $select = date('Y-m');
        } else {
            $select = date('Y-m', strtotime($select));
        }

        $event = $this->event_date_diff($this->instance->event->get_by_month($select));

        $calendar_array = array();

        $date = date('Y-m-d', strtotime($select));

        //จำนวนวันในเดือน จากตัวแปร date
        $number_day = date('t', strtotime($date));
        //ตัวแปรเก็บวัน
        $day = date('d', strtotime($date));
        //ตัวแปรเก็บเดือน
        $month = date('m', strtotime($date));

        //ตัวแปรเก็บ ปี
        $year = date('Y', strtotime($date));

        $date_select = date('Y-m', strtotime($select));

        if (date('Y-m', strtotime($date)) != $date_select) {
            continue;
        }

        $week = 0;

        for ($i = 1; $i <= $number_day; $i++) {
            //เก็บว่าวันที่ 1 เป็น วันจันทร์
            $tmp      = mktime(0, 0, 0, $month, $i, $year);
            $tmp_date = date('Y-m-d', $tmp);
            $dow      = date('w', $tmp);
            if ($dow == 0) {
                $week++;
            }
            $calendar_array[$week][$dow]['day']  = date('d', $tmp);
            $calendar_array[$week][$dow]['date'] = date('Y-m-d', $tmp);

            if ($tmp_date == date('Y-m-d')) {
                $calendar_array[$week][$dow]['type'] = 'today';

                $calendar_array[$week][$dow]['today'] = true;
            }

            if (isset($event[$tmp_date])) {
                // เพิ่ม กิจกรรมใน อารเรย์ โดยใช้ index เป็นวันที
                if(isset($calendar_array[$week][$dow]['type'])){
                    $calendar_array[$week][$dow]['type'] = 'both';
                }else{
                    $calendar_array[$week][$dow]['type'] = 'event';
                }
                $calendar_array[$week][$dow]['event'] = $event[$tmp_date];
            }

        }

        foreach ($calendar_array as $week_key=>$week) {
            for ($dow = 0; $dow < 7; $dow++) {
                if (isset($week[$dow]['event'])) {
                    $return[$week_key][$dow] = $week[$dow];
                } else if (isset($week[$dow]['day'])) {
                    if (isset($week[$dow]['today'])) {
                        $return[$week_key][$dow] = $week[$dow];
                    } else {
                        $return[$week_key][$dow] = $week[$dow];
                    }
                } else {
                   $return[$week_key][$dow] = array('type'=>'blank');
                }
            }
        }

        // ส่งค่ากลับไป
        return $return;
        
    }

    public function week($select)
    {
        if ($select == false) {
            $select = date('Y-m-d');
        } else {
            $select = date('Y-m-d', strtotime($select));
        }

        $event_list = $this->instance->event->get_by_week($select);

        $return = array();

        foreach ($event_list as $ev) {

            $start = strtotime($ev->event_start);
            $end = strtotime($ev->event_end);

            $offset  = date('H',$start)+(date('w',$start)*24);

            $diff = ceil(($end-$start)/3600);

            for($i = 0;$i <= $diff ;$i++){
                $return[($offset+$i)][] = $ev->to_array();
            }
        }

        return $return;
    }

    public function day($select)
    {
        if ($select == false) {
            $select = date('Y-m-d');
        } else {
            $select = date('Y-m-d', strtotime($select));
        }

        $event_list = $this->instance->event->get_by_day($select);

        $return = array();

        foreach ($event_list as $ev) {

            $start = strtotime($ev->event_start);
            $end = strtotime($ev->event_end);

            $offset = date('H',$start);

            $diff = ceil(($end-$start)/3600);

            for($i = 0;$i <= $diff ;$i++){
                $return[$offset+$i][] = $ev->to_array();
            }
            
        }

        return $return;
    }    

}
?>