<?php
abstract class model{

	protected $instance;
    protected $error;

	public function __construct($array  = false){
		$this->instance = get_instance();

        if($array == true){
            $this->set($array);
        }
	}

    public function get_error(){
        return $this->error;
    }

    public function set($array){
        foreach($array as $key=>$value){
            $this->$key = $value; 
        }

        return $this;
    }

    public function fill($array){

        if($array == true){
            foreach($array as $key=>$value){
                if(empty($this->$key)){
                    $this->$key = $value;  
                }     
            }        
        }
    }
   
    public function __get($key){
        if(isset($this->object[$key])){
            return $this->object[$key];
        }
    }    

    public function __set($key,$value){
        
        if(method_exists($this,$key) == true){
            $value = call_user_func_array(array($this,$key),array($value));
        }

        if(isset($this->object[$key])){
           return $this->object[$key] = $value;
        }

        return false;
        
    }

	public function to_array(){
        return $this->object; 
	}

}