<?php
Class error{

	private $instance;

	public function __construct()
	{
		$this->instance = get_instance();
	}

	public function page_not_found()
	{
		$this->instance->template->set_view('body','error/error_404');
	}



}