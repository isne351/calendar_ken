<?php

session_start();

// dev state
	error_reporting(E_ALL);
	ini_set('display_errors',1);
//

ini_set('date.timezone', 'Asia/Bangkok');

require_once './config/config.php';
require_once './config/database.php';
require_once './config/route.php';

require_once './system/library/loader.php';
require_once './system/library/instance.php';
require_once './system/library/database.php';
require_once './system/library/input.php';
require_once './system/library/route.php';

require_once './system/library/library.php';
require_once './system/library/model.php';
require_once './system/library/control.php';

require_once './system/helper/common.php';

define('__BASE',realpath(dirname(__FILE__)));// กำหนดพาทหลักของระบบ
define('__URL',$config['url']);// กำหนดพาทหลักของระบบ

$instance = &new instance();
$loader = &new instance();

$instance->loader = $loader;
$instance->loader->database = new database();
$instance->loader->system = new loader(__BASE.'/system/');
$instance->loader->app = new loader(__BASE.'/application/');

$route = new route($config['route']);
echo $route->exec(input::get('route'));
