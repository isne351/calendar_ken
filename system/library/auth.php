<?php
class auth{
	public function __construct(){
		$this->instance = &get_instance();

		$this->instance->loader->app->model('user');

		$this->instance->loader->database->connect();

		if(input::session('login') == true){
			$this->instance->user->user_id = input::session('login');
			$this->instance->user->check_login();
		}

	}
}
?>