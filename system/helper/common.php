<?php
function &get_instance(){
	global $instance;
	return $instance;
}

function get_config($type){
	global $config;
	return $config[$type];
}

function base($url = ''){
	return __URL.$url;
}

function redirect($url){
	header('location:'.$url);
}

function alert($url,$message){
	$_SESSION['alert'] = $message;
	header('location:'.$url);
}

function result($message){
	$_SESSION['alert'] = $message;
	header('location:'.$_SERVER['HTTP_REFERER']);	
}